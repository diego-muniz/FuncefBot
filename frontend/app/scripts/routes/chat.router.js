﻿(function (){
    'use strict';

    angular.module('botframework')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('home', {
                    url: '/',
                    ncyBreadcrumb: {
                        label: 'Home'
                    },
                    redirectTo: 'chat'
                })

                .state('chat', {
                    url: '/chat',
                    templateUrl: 'pages/chat/chat.view.html',
                    controller: 'ChatController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Chat'
                    }
                })
        });
})();
