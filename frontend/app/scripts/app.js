﻿(function () {
    'use strict';

    angular.module('botframework.configuration', []);
    angular.module('chat.controller', []);

    angular
        .module('botframework', [
            'botframework.configuration',
            'chat.controller',
            'ui.router',
            'ncy-angular-breadcrumb',
            'ngCookies'
        ]);
})();
