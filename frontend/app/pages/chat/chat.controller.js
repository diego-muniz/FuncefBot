﻿(function () {
    'use strict';

    angular.module('chat.controller')
        .controller('ChatController', ChatController);

    ChatController.$inject = [];

    function ChatController($scope) {
        var vm = this;

        vm.abrirChat = abrirChat;

        init();

        function init(){
            vm.chat = false;
        }

        function abrirChat() {
            vm.chat = true;
        }
    }

})();
