(function () {
    'use strict';

    angular.module('botframework.configuration', []);
    angular.module('chat.controller', []);

    angular
        .module('botframework', [
            'botframework.configuration',
            'chat.controller',
            'ui.router',
            'ncy-angular-breadcrumb',
            'ngCookies'
        ]);
})();
;(function (){
    'use strict';

    angular.module('botframework')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');

            $urlRouterProvider.otherwise('/404');

            $stateProvider

                .state('home', {
                    url: '/',
                    ncyBreadcrumb: {
                        label: 'Home'
                    },
                    redirectTo: 'chat'
                })

                .state('chat', {
                    url: '/chat',
                    templateUrl: 'pages/chat/chat.view.html',
                    controller: 'ChatController',
                    controllerAs: 'vm',
                    ncyBreadcrumb: {
                        label: 'Chat'
                    }
                })
        });
})();
;angular.module('').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('pages/chat/chat.view.html',
    "<h1>Web Chat</h1> <iframe ng-if=\"vm.chat == true\" height=\"400px\" class=\"chat\" src=\"https://webchat.botframework.com/embed/BotAir?s=scQGZHKs8HY.cwA.CTM.vnUxszP6xu7OwnFZzjqmSedWg5aEvG6nLo7_I38Xj2Q\"></iframe> <div class=\"abrirChat\" ng-if=\"vm.chat == false\" ng-click=\"vm.abrirChat()\"> <div> <i class=\"fa fa-comments-o\" aria-hidden=\"true\"></i> </div> <div> <span>Precisando de ajuda?</span> </div> </div>"
  );

}]);
